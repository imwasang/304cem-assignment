### 運行步驟
1. 開啟Mysql數據庫
2. 運行
```bash
$ npm install
$ node app.js
```

### 目錄結構
controller ==> 操作數據庫的文件

routes ==> 路由控制

data ==> Recipe數據表的數據

config.json ==> 首次使用的配置文件
```js
{
    // 數據庫如果沒有創建就設置false，true就表示已經創造
    "init": false,
    // 數據庫配置 database裡的database不寫
    "database": {
        "host": "localhost",
        "user": "root",
        "port": 3306,
        "password": "",
        "database": ""
    }
}
```

sql.js ==> 創建數據表和創建admin用戶的sql語句

public ==> 前端靜態文件

public/image copy ==> 圖片備份

### 後端api地址
|  作用  | url | method | 
|---|---|---|---|---|
|  獲取全部食譜列表     | /api/recipe | GET | 
|  添加單個食譜     | /api/recipe | POST | 
|  修改單個食譜     | /api/recipe | PUT | 
|  刪除單個食譜     | /api/recipe/:RecipeId | DELETE |
|  獲取單個食譜詳細信息     | /api/recipe/:RecipeId | GET | 
|  獲取用戶喜歡的食譜     | /api/love/:UserID | GET |
|  刪除用戶喜歡的食譜其中一個     | /api/love/:UserId/:RecipeId | DELETE | 
|  註冊     | /api/user/register | POST | 
|  登錄    | /api/user/login | POST | 
|  驗證是否登錄     | /api/user/validate | GET |
|  登出     | /api/user/logout | GET | 