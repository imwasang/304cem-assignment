var mysql = require('mysql');
var tableSql = require('./sql');
var fs = require('fs');
var reciepeData = require('./data/recipe.json')
var config = require('./config.json');

let con = mysql.createPool(config.database);

con.query('create database `RecipeDB` character set utf8 collate utf8_general_ci;', function (err) {
    config.database.database = 'RecipeDB'
    con = mysql.createConnection(config.database)
    tableSql.forEach(function (sqlItem) {
        con.query(sqlItem, function (err) { });
    })
    if (config.init == false) {
        reciepeData.forEach(function (item) {
            let keys = Object.keys(item).toString();
            let values = Object.values(item).map(function (item) {
                return `"${item}"`
            }).toString();
            con.query(
                `
                insert into recipe(${keys}) values(${values})
            `,
                function (err) {
                    config.init = true;
                    fs.writeFile('./config.json', JSON.stringify(config, null, 4), function (err) { })
                }
            )
        })


    }
})


module.exports = {
    /**
     * 
     * 根據數據表名稱查找出所有的數據
     * @param {String} tableName 數據表名稱
     * @param {Array<string>} fields 需要什麼字段，空數代表*
     * @param {Function} cb 回調函數
     */
    getAll(tableName, fields = [], cb) {
        const sql = `select ${fields.length ? fields.toString() : '*'} from ${tableName}`
        con.query(sql, [], cb);
    },
    /**
     * 根據where條件查詢
     * @param {String} tableName 數據表名稱
     * @param {Object} data where需要的數據 類似: {id:2}
     * @param {Function} cb 回調函數
     */
    getAllByFilter(tableName, data, cb) {

        /**
         * 需要的sql格式
         *  select * from Person where id=? and username=?
         */

        let keywords = Object.keys(data)
        let vals = Object.values(data);

        // 拼接成 id=? and username=? 格式, 最後一個沒有'=? and '直接 加上 '=?' 就行
        keywords = keywords.join('=? and ') + '=?';
        const sql = `select * from ${tableName} where ${keywords}`;
        // 直接傳遞vals即可, vals是一個數組, 按順序對應的?號
        con.query(sql, vals, cb);
    },
    /**
     * 根據id查詢
     * @param {String} tableName 數據表名稱
     * @param {Object} data 只要是帶id的這樣傳入{ xxId: 1 }
     * @param {Function} cb 回調函數
     */
    getOneById(tableName, data, cb) {
        let sql = `select * from ${tableName} where ${data.join('=')}`;
        con.query(sql, [], cb);
    },
    /**
     *
     * @param {String} tableName 數據表名稱
     * @param {Object} data 修改後的值可以有多個: { RecipeName: 'newName' }
     * @param {Object} where where條件: { RecipeId: 1 }, 可以只傳入{}空對象代表無where條件
     * @param {Function} cb 回調函數
     */
    update(tableName, data, where, cb) {

        let setArr = []
        let newWhere = []
        Object.keys(data).forEach(function (key) {
            setArr.push(`${key}="${data[key]}"`)
        })

        if (Object.keys(where).length > 0) {
            Object.keys(where).forEach(function (key) {
                newWhere.push(`${key}="${where[key]}"`)
            })
            newWhere = ' where ' + newWhere.join(' and ')
        }
        const sql = `update ${tableName} set ${setArr.join(',')}${newWhere.toString()}`
        con.query(sql, [], cb);
    },
/**
     * 插入一條數據
     * @param {String} tableName 數據表名稱
     * @param {Object} data 例如：{ RecipeId:1,RecipeName:'John' } 種類型的數據, 對應的是數據表表中字段名和字段值
     * @param {Function} cb 回調函數
     */
    add(tableName, data, cb) {
        let keywords = Object.keys(data);
        let vals = Object.values(data);
        let questions = Object.keys(data).map(function () {
            return '?'
        });
        keywords = keywords.join(', ');
        questions = questions.join(',');
        const sql = `insert into ${tableName} (${keywords}) values(${questions})`;
        con.query(sql, vals, cb);
    },
    remove(tableName, where, cb) {
        let sql = '';
        let newWhere = []
        if (where) {
            Object.keys(where).forEach(function (key) {
                newWhere.push(`${key}="${where[key]}"`)
            })
            newWhere = ' where ' + newWhere.join(' and ')
        }
        sql = `delete from ${tableName}${newWhere}`
        con.query(sql, [], cb);
    },
/**
     *
     * 獲取喜歡的列表
     * @param {Number} UserID
     * @param {Function} cb
     */
    getFavourite(UserID, cb) {
        let where = ''
        if (UserID) {
            where = ` where u.UserID = ${UserID}`
        }
        let sql = `
            select c.UserID, c.RecipeId, c.AddTime, r.RecipeName, r.RecipePicture, r.RecipeTime,r.RecipeCategory from love c 
            left join user u on u.UserID = c.UserID 
            left join recipe r on r.RecipeId = c.RecipeId${where} 
            `;
        con.query(sql, [], cb);
    }
}


